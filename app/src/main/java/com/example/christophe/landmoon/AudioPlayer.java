package com.example.christophe.landmoon;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by christophe on 3/31/15.
 */
public class AudioPlayer {
    private MediaPlayer mPlayer;

    public void stop() {
        if (mPlayer != null )
        {
            // destroy the instance
            mPlayer.release();
            mPlayer = null;
        }
    }

    // Keep exactly one MediaPlayer around
    // and keep it around only as long as it is playing
    public void play (Context c)
    {
        // initial call to stop()
        stop();
        mPlayer = MediaPlayer.create(c, R.raw.one_small_step);

        // set a listener to call stop() when the audio file has finished playing
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
        {
            public void onCompletion(MediaPlayer mp)
            {
                stop();
            }
        });

        mPlayer.start();
    }

    public boolean isPlaying()
    {
        return mPlayer != null;
    }
}
